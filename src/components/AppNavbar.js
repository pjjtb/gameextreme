import React,{useContext} from 'react';

import {Navbar,Nav} from 'react-bootstrap';

import {Link} from 'react-router-dom';

import UserContext from '../userContext';

import Swal from 'sweetalert2';


export default function AppNavbar(){


	const {user,setUser} = useContext(UserContext);

	const totalNumberOfItems = user.cart ? user.cart.reduce((accumulator, item) =>{

		return accumulator = accumulator + item.quantity
	},0):0;
	console.log(user);

	function logout(){

		Swal.fire({

					icon: "success",
					title: "Let's G Later!",
					text:  "Logout Successfull"
				})
	}

	return (
		<>
		<Navbar expand="lg">
				<Navbar.Brand as={Link} to="/">🎮GameExtreme</Navbar.Brand>
				<Navbar.Toggle aria-controls="basic-navbar-nav"/>
				<Navbar.Collapse id="basic-navbar-nav">
					<Nav className="ml-auto">
						<Nav.Link className="text-info" as={Link} to="/"><strong>| 🏠Home |</strong></Nav.Link>
						<Nav.Link className="text-info" as={Link} to="/products"><strong>| 🕹️Products |</strong></Nav.Link>
						
						{

						user.id
						?
							user.isAdmin === true 
							?
							<>
							<Nav.Link className="text-info" as={Link} to="/addProducts"><strong>| 🛠️Create |</strong></Nav.Link>
							<Nav.Link className="text-info" as={Link} to="/logout" onClick={logout}><strong>| 🚪Logout |</strong></Nav.Link>
							</>
							:
							<>
							<Nav.Link className="text-info" as={Link} to="/cart"><strong>| 🛒Cart({totalNumberOfItems}) |</strong></Nav.Link>
							<Nav.Link className="text-info" as={Link} to="/orders"><strong>| 📦Orders |</strong></Nav.Link>
							<Nav.Link className="text-info" as={Link} to="/logout" onClick={logout}><strong>| 🚪Logout |</strong></Nav.Link>

							</>
						
							
						:
						<>
							<Nav.Link className="text-info" as={Link} to="/register"><strong>| 👤Register |</strong></Nav.Link>
							<Nav.Link className="text-info" as={Link} to="/login"><strong>| 👥Login |</strong></Nav.Link>
						</>
						}
					
							
						
					</Nav>
				</Navbar.Collapse>

			</Navbar>
			
			<hr className="ruler"/>

			</>
	)
}