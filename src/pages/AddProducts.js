import React,{useState,useEffect,useContext} from 'react';
import {Form,Button} from 'react-bootstrap';

import Swal from 'sweetalert2';

import {Redirect,useHistory} from 'react-router-dom';

import UserContext from '../userContext';

export default function AddProduct(){

	const{user} = useContext(UserContext);

	const [name,setName] = useState("");
	const [description,setDescription] = useState("");
	const [price,setPrice] = useState("");
	const [genre,setGenre] = useState("");

	const [isActive,setIsActive] = useState(false);

	const history = useHistory();

	useEffect(()=>{


		if(name !== "" && description !== "" && price !== 0 && genre !== ""){

			setIsActive(true);
		} else {

			setIsActive(false);
		}


	},[name,description,price,genre])

	function createProduct(e){

		e.preventDefault();

		fetch('https://serene-woodland-75361.herokuapp.com//products/createP',{

			method: 'POST',
			headers: {

				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			},

			body: JSON.stringify({

				name: name,
				description: description,
				genre: genre,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {

			if(data.message){

				Swal.fire({
					icon: "error",
					title: "Cannot Add Game",
					text: data.message
					
				})
			} else {

				Swal.fire({
					icon: "success",
					title: "Game Added Successfully!",
					text: "Game was Added"
				})
				history.push('/products')
			}
		})
		setName("");
		setDescription("");
		setPrice(0);
		setGenre("");
	};

	return (

		<>
			<h1 className="text-center my-5">Product Creation</h1>
			<Form onSubmit={e => createProduct(e)}>
				<Form.Group>
					<Form.Label>Name: </Form.Label>
					<Form.Control type="text" value={name} onChange={e => {setName(e.target.value)}} placeholder="Game Name" required></Form.Control>
				</Form.Group>
				<Form.Group>
					<Form.Label>Description: </Form.Label>
					<Form.Control type="text" value={description} onChange={e => {setDescription(e.target.value)}} placeholder="Details" required></Form.Control>
				</Form.Group>
				<Form.Group>
					<Form.Label>Genre: </Form.Label>
					<Form.Control type="text" value={genre} onChange={e => {setGenre(e.target.value)}} placeholder="Game Genre" required></Form.Control>
				</Form.Group>
				<Form.Group>
					<Form.Label>Price: </Form.Label>
					<Form.Control type="text" value={price} onChange={e => {setPrice(e.target.value)}} placeholder="PHP 0.00" required></Form.Control>
				</Form.Group>
				{

					isActive
					?
					<Button className="mb-3" variant="warning" type="submit">Create</Button>
					:
					<Button className="mb-3" variant="danger" type="submit" disabled>Create</Button>
				}
			</Form>
		</>

	)
}