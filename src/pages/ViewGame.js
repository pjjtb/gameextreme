import React,{useState,useContext,useEffect} from 'react';

import {Card,Button,Row,Col,Jumbotron} from 'react-bootstrap';

import {useParams,useHistory,Link} from 'react-router-dom';

import Swal from 'sweetalert2';

import UserContext from '../userContext';

import Product from '../components/Products'

export default function ViewGame(){


	const {productId} = useParams();

	const {user,addToCart} = useContext(UserContext);

	const history = useHistory();

	const [gameDetails, setGameDetails] = useState({

		name: null,
		description: null,
		price: null
	})

	useEffect(() => {

		fetch(`https://serene-woodland-75361.herokuapp.com/products/getSingleP/${productId}`)
		.then(res => res.json())
		.then(data => {

			if(data.message){

				Swal.fire({
					icon: "error",
					title: "Game Unavailable",
					text: data.message
				})

			} else {

				setGameDetails({

					id: data._id,
					name: data.name,
					description: data.description,
					genre: data.genre,
					price: data.price


				})

			}

		})



	},[productId])
	
	
	return (

		<Row>
			<Col>
				<Jumbotron className="text-center mt-5 viewJumbo">

				<h4 className="text-left mb-5 txtColorGame">PHP {gameDetails.price}</h4>
				<h1 className="txtColorGame">{gameDetails.name}</h1>
				<h4 className="mt-5 txtColorGame">{gameDetails.genre}</h4>
				<p className="my-5 txtColorGame">{gameDetails.description}</p>

				{
							user.isAdmin === false 
							?
							<>
							<Button className="btn btn-info mx-3" onClick={()=>addToCart(gameDetails)}> 💸Buy Game</Button>
							<Link className="btn btn-light" to="/products">Go Back</Link>
							</>
							:
							<Link className="btn btn-info btn-lg" to="/login">Login</Link>
				}
					
				</Jumbotron>
			</Col>
		</Row>
			
	)
}