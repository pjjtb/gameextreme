import React,{useState,useEffect,useContext} from 'react';

import {useHistory,useParams} from 'react-router-dom';

import UserContext from '../userContext'

import Swal from 'sweetalert2';

import {Form, Button} from 'react-bootstrap';

export default function UpdateProduct(props){


	const {user} = useContext(UserContext);

	const history = useHistory();

	const {productId} = useParams();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [genre,setGenre] = useState("");

	// const [id, setId] = useState(0);


	const [isActive, setIsActive] = useState(false);
	useEffect(() => {

		if(name !== "" && price !== 0 && description !== "" && genre !== ""){

			setIsActive(true);
		} else {

			setIsActive(false);
		}

	},[name,price,description,genre])



	function updateProduct(e){

		e.preventDefault();


		fetch(`https://serene-woodland-75361.herokuapp.com/products/updateP/${productId}`,{

			method: 'PUT',
			headers: {

				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({

				name: name,
				description: description,
				genre: genre,
				price: price
			})
		})
		
		.then(res => res.json())
		.then(data => {

			if(data.message){

				Swal.fire({

					icon: "error",
					tittle: "Failure",
					text: data.message
				})

			} else {

				Swal.fire({

					icon: "success",
					title: "Updated",
					text:  "Game is now Updated!"
				})
				history.push('/products')

			}
		})



	}

	return (

		<>
			<h1 className="text-center my-5">Update Product</h1>
			<Form onSubmit={e => updateProduct(e)}>
				<Form.Group>
					<Form.Label>Name Update: </Form.Label>
					<Form.Control type="text" value={name} onChange={e => {setName(e.target.value)}} placeholder="---" required></Form.Control>
				</Form.Group>
				<Form.Group>
					<Form.Label>Description Update: </Form.Label>
					<Form.Control type="text" value={description} onChange={e => {setDescription(e.target.value)}} placeholder="---" required></Form.Control>
				</Form.Group>
				<Form.Group>
					<Form.Label>Genre Update: </Form.Label>
					<Form.Control type="text" value={genre} onChange={e => {setGenre(e.target.value)}} placeholder="---" required></Form.Control>
				</Form.Group>
				<Form.Group>
					<Form.Label>Price Update: </Form.Label>
					<Form.Control type="text" value={price} onChange={e => {setPrice(e.target.value)}} placeholder="---" required></Form.Control>
				</Form.Group>
				{
					isActive
					?
					<Button variant="info" className="mb-3" type="submit">Update</Button>
					:
					<Button variant="danger" className="mb-3" type="submit" disabled>Update</Button>
				}
			</Form>
		</>

	)

}