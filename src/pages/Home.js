import React,{useEffect,useState} from 'react';

import Banner from '../components/Banner';

import {Link} from 'react-router-dom';

import {Card,Button,Row,Col} from 'react-bootstrap';



export default function Home(){

	const [products, setProducts] = useState([]);

	let bannerComponent = {

		title: "Gaming Shop",
		description: "The Place to Buy Games",
		buttonToCall: "Buy Now!",
		destination: "/login"
	};

		useEffect(()=>{

		fetch('https://serene-woodland-75361.herokuapp.com/products/getP')
		.then(res => res.json())
		.then(data => {

			setProducts(data.map(product => {

				if(product.price > 2000){

						return (

					
					<div key={product._id} className="text-center mb-5 crdHome">
					<Row >
						<Col>
							<Card crdHome>
								<Card.Header>Featured</Card.Header>
								<Card.Body>
									<Card.Title>{product.name}</Card.Title>
									<Card.Text>{product.genre}</Card.Text>
								</Card.Body>
							<Card.Footer className="text-muted">
  								<Link className="btn btn-danger" to={`/products/${product._id}`}>View Game</Link>
  							</Card.Footer>
							</Card>
						</Col>
					</Row>
					</div>
					

				)

				}
			
			}))
		})


	},[])

	return (
 
		<>

		<Banner bannerProp={bannerComponent}/>

		<h1 className="text-center my-5 text-danger">Featured Products</h1>

		{products}
		
		</>

	)

}