import React,{useState,useEffect,useContext} from 'react';

import {Card,Row,Col,Jumbotron} from 'react-bootstrap';

import Order from '../components/Order';

import Swal from 'sweetalert2';

import UserContext from '../userContext';

export default function  UserOrder(){


	const {user,emptyCart} = useContext(UserContext);

	console.log(user);


	const [orderDetails, setOrderDetails] = useState([])

	
	useEffect(() =>{


		fetch(`https://serene-woodland-75361.herokuapp.com/orders/retrieveO`,{

			headers: {

				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {

			let sample = data.map(order => {

			
				return (
					<Order key={order._id} orderProp={order}/>
				)
			})

			
			setOrderDetails(sample)

		})



	},[])

	console.log(orderDetails);

	return (

	
		
	

		
		<>
		<h1 className="text-center my-5">Order Page</h1>
		<div>
		{orderDetails}
			
		</div>
		</>
		

	)
}