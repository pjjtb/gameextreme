import React,{useContext,useEffect} from 'react';
import UserContext from '../userContext';

import Banner from '../components/Banner';

export default function Logout(){

	const{setUser,unsetUser} = useContext(UserContext);

	unsetUser();

	useEffect(()=>{

		setUser({

			id: null,
			isAdmin: null,
			cart: []
		})


	},[])

	const bannerComponent = {


		title: "Be well, do good work, and keep in touch",
		description: "You have logged out of GameExtreme",
		buttonToCall: "Go Back to Home Page",
		destination: "/"
	}

	return (

		<Banner bannerProp={bannerComponent}/>

	)
}