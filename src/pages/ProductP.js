import React,{useState,useEffect,useContext} from 'react';

import Product from '../components/Products';

import {Link,useHistory} from 'react-router-dom';

import UserContext from '../userContext';
import {Table,Button} from 'react-bootstrap';

export default function Products(){

	const {user} = useContext(UserContext);

	const [productsArray, setProductsArray] = useState([]);

	const [allProducts, setAllProducts] = useState([]);

	const [update, setUpdate] = useState(0);

	const history = useHistory();

	


	useEffect(()=>{

		fetch('https://serene-woodland-75361.herokuapp.com/products/getP')
		.then(res => res.json())
		.then(data => {

			setProductsArray(data.map(product => {

				return (

					<Product key={product._id} productProp={product}/>
				)
			}))
		})


	},[])



	useEffect(()=>{

		if(user.isAdmin){

			fetch('https://serene-woodland-75361.herokuapp.com/products/allP',{

				headers: {

					'Authorization': `Bearer ${localStorage.getItem('token')}`
				}
			})
			.then(res => res.json())
			.then(data => {

				console.log(data);

				setAllProducts(data.map((product => {

					return (

						<tr key={product._id}>

							<td>{product._id}</td>
							<td>{product.name}</td>
							<td>{product.price}</td>
							<td>{product.isActive ? "Active" : "Inactive"}</td>
							<td>{

								product.isActive
								?
								<>
								<Button variant="danger" className="mx-2" onClick={()=>{


									archive(product._id)
									
								}}>❌Archive</Button>
								<Link className="btn btn-warning" to={`/updateProducts/${product._id}`}>Update</Link>
								</>
								:
								<>
								<Button variant="success" className="mx-2" onClick={()=>{

									activate(product._id)
								}}>✔️Activate</Button>
								<Link className="btn btn-warning" to={`/updateProducts/${product._id}`}>Update</Link>
								</>
							}
							</td>
							
						</tr>

					)

				})))
			})
		}



	},[user,update])

	function archive(productId){

		fetch(`http://localhost:4000/products/archiveP/${productId}`,{

			method: 'PUT',

			headers: {

				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})

		.then(res => res.json())
		.then(data => {

			setUpdate(update+ 1)
		})

	}

	function activate(productId){

		fetch(`http://localhost:4000/products/activateP/${productId}`,{

			method: 'PUT',
			headers: {

				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {

			setUpdate(update+1)
		})
	}


	return (
		user.isAdmin 
		?
		<>

			<h1 className="my-5 text-center">Games Dashboard</h1>
			<Table stripped bordered hover className="text-center">
				<thead className="text-white bg-danger">
					<tr>
						<th>Game ID</th>
						<th>Game</th>
						<th>Price</th>
						<th>Status</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{allProducts}
				</tbody>
			</Table>

		</>
		:
		<>
		<h1 className="my-5 text-center">Games List</h1>
		{productsArray}
		</>

	)
}