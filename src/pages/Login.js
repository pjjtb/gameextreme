import React,{useState,useEffect,useContext} from 'react';

import {Form,Button} from 'react-bootstrap';

import Swal from 'sweetalert2';
import UserContext from '../userContext';

import {Redirect} from 'react-router-dom';

export default function Login(){

	const {user,setUser} = useContext(UserContext);

	const[email,setEmail] = useState("");
	const[password,setPassword] = useState("");

	const[isActive, setIsActive] = useState(false);

	useEffect(()=>{

		if(email !== "" && password !== ""){

			setIsActive(true);
		
		} else {

			setIsActive(false);
		}



	},[email,password])

	function loginUser(e){

		e.preventDefault();

		fetch('https://serene-woodland-75361.herokuapp.com/users/login',{

			method: 'POST',
			headers: {

				'Content-Type': 'application/json'
			},
			body: JSON.stringify({

				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			if(data.accessToken){

			
				localStorage.setItem('token', data.accessToken)
				fetch('https://serene-woodland-75361.herokuapp.com/users/userDetails',{

					headers: {

						'Authorization': `Bearer ${data.accessToken}`
					}
				})

				.then((res) => {
              	console.log(res)
              	return res.json()
            	})
				.then(data => {

					console.log(data)

					Swal.fire({
					icon: "success",
					title: "Login Successful!",
					text: `Welcome back ${data.firstName} ${data.lastName}!`
				})
					setUser({

						id: data._id,
						isAdmin: data.isAdmin,
						cart: []
					})
				})
			} else {
				Swal.fire({
					icon: "error",
					title: "Login Failure!",
					text: data.message
				})
			}
		})
	}

	return (

		user.id
			?
			<Redirect to="/products"/>
			:
			<div className="registerForm mx-auto mt-5">	
			<h1 className="text-center my-5">Login Page</h1>
			<Form onSubmit={e => loginUser(e)}>
				<Form.Group>
					<Form.Label>Email:</Form.Label>
					<Form.Control type="email" value={email} onChange={e => {setEmail(e.target.value)}} placeholder="123@email.com" required/>
				</Form.Group>
				
				<Form.Group>
					<Form.Label>Password:</Form.Label>
					<Form.Control type="password" value={password} onChange={e => {setPassword(e.target.value)}}placeholder="Enter Password" required/>
				</Form.Group>
				
				{
					isActive
					? <Button className="btn-block mb-3" variant="primary" type="submit">Login</Button>
					: <Button className="btn-block" variant="secondary" disabled>Login</Button>
				}
			</Form>

		</div>
	

	)

}