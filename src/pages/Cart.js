import React,{useContext} from 'react';

import UserContext from '../userContext';

import {Button,Card,Table,Jumbotron} from 'react-bootstrap';

import Swal from 'sweetalert2';

import {useHistory} from 'react-router-dom'


export default function Cart(){

	const {user,emptyCart} = useContext(UserContext);

	const {cart} = user;

	const history = useHistory();



	

	
	function checkOut(e){



		e.preventDefault();

		const products = cart.map((item)=>{


			return {productId: item.id, quantity: item.quantity}
		})

		fetch(`https://serene-woodland-75361.herokuapp.com/orders/createO/`,{

			method: 'POST',
			headers: {

				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({

				products
			})
		})
		
		.then(res => res.json())
		.then(data => {

			if(!data.id){

				Swal.fire({

					icon: "error",
					tittle: "Failure",
					text: data.message
				})

			} else {

				emptyCart();
				
				Swal.fire({

					icon: "success",
					title: "Success",
					text:  "Order Successfull!"
				})
				history.push('/orders')

			}
		})
		.catch(e => {

			console.error(e);
			Swal.fire({

					icon: "error",
					tittle: "Failure",
					text: "Something went wrong"
				})
		})

	
	}

	console.log(user)
	return (
		
		user.cart.length === 0
		?
		<>
		<h1 className="my-5 text-center">Cart Dashboard</h1>

			<Table stripped bordered hover className="text-center tableCart bg-info text-white">
				<thead>
					<tr>
						<th>Game ID</th>
						<th>Game</th>
						<th>Price</th>
						<th>Quantity</th>
						<th>TotalAmount</th>
						
					</tr>
				</thead>
		</Table>
		<Jumbotron className="text-center">
				<h1>Empty Cart</h1>
		</Jumbotron>
	
		</>
		:
		<div>

			
			

			<h1 className="my-5 text-center">Cart Dashboard</h1>
			<Table stripped bordered hover className="text-center tableCart">
				<thead>
					<tr>
						<th>Game ID</th>
						<th>Game</th>
						<th>Price</th>
						<th>Quantity</th>
						<th>Total Amount</th>
					
					</tr>
				</thead>
				<tbody>

					
					
						{cart.map((item)=>{

					return (
						<>
						<tr key={item.id}>
							
							<td>{item.id}</td>
							<td>{item.name}</td>
							<td>{item.price}</td>
							<td>{item.quantity}</td>
							<td>{item.price * item.quantity}</td>
							
							
						</tr>
						
							
						
						</>
						)
				})
			}
							
				</tbody>
			</Table>
			<div className="text-center mt-5">
				<Button variant="danger" onClick={emptyCart}>Empty Cart</Button>
				<Button className="mx-3" variant="info" onClick={checkOut}>Checkout</Button>
			</div>
			
		</div>
		
		
	)
}