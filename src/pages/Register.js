import React, {useState,useEffect,useContext} from 'react';

import {Form,Button,Container} from 'react-bootstrap';

import Swal from 'sweetalert2';

import UserContext from '../userContext';

import {Redirect,useHistory} from 'react-router-dom';

export default function Register(){

	const {user} = useContext(UserContext);

	const history = useHistory();

	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [password, setPassword] = useState("");
	// const [verify, setVerify] = useState("");

	const [isActive, setIsActive] = useState(false);

	useEffect(() =>{

		if((firstName !== "" && lastName !== "" && email !== "" && mobileNo !== "" && password !== "") && (mobileNo.length === 11)){

			setIsActive(true);
		} else {

			setIsActive(false);
		}



	},[firstName,lastName,email,password,mobileNo])


function registerUser(e){

		//prevent submit event's default behavior
		e.preventDefault();

	

		fetch('https://serene-woodland-75361.herokuapp.com/users/register',{

			method: 'POST',
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo,
				password: password
			})

		})
		.then(res => res.json())
		.then(data => {

			console.log(data);
			
			if(data.email){

				Swal.fire({

					icon: "success",
					title: "Registration Succesful!",
					text: `Thank you for your registering, ${data.firstName} ${lastName}`

				})

				
				history.push('/login')

			} else {

				Swal.fire({

					icon: "error",
					title: "Failure",
					text: "Registration Unsuccessfull!"

				})

			}
		})

	}


	return (
		user.id
		?
		<Redirect to="/products"/>
		:
		<div className="registerForm mx-auto mt-5">
	
				<h1 className="my-5 text-center">Register</h1>
				<Form onSubmit={e => registerUser(e)}>
				<Form.Group>
					<Form.Label>First Name:</Form.Label>
					<Form.Control  size="sm" type="text" value={firstName} onChange={e => {setFirstName(e.target.value)}} placeholder="Name" required/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Last Name:</Form.Label>
					<Form.Control type="text" value={lastName} onChange={e => {setLastName(e.target.value)}}placeholder="Last Name" required/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Email:</Form.Label>
					<Form.Control type="email" value={email} onChange={e => {setEmail(e.target.value)}} placeholder="123@email.com" required/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Mobile No:</Form.Label>
					<Form.Control type="number" value={mobileNo} onChange={e => {setMobileNo(e.target.value)}}placeholder="11 digit Number" required/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Password:</Form.Label>
					<Form.Control type="password" value={password} onChange={e => {setPassword(e.target.value)}}placeholder="Enter Password" required/>
				</Form.Group>
				{
					isActive
					? <Button className="btn-block mb-3" variant="primary" type="submit">Register</Button>
					: <Button className="btn-block mb-3" variant="secondary" disabled>Register</Button>
				}
				
	
			</Form>
		

		</div>

	)
}


