import React,{useState,useEffect} from 'react';

import {BrowserRouter as Router} from 'react-router-dom';

import {Route, Switch} from 'react-router-dom';

import {UserProvider} from './userContext'

import AppNavbar from './components/AppNavbar';

import Swal from 'sweetalert2';

import Home from './pages/Home';
import Login from './pages/Login';
import Register from './pages/Register';
import ProductP from './pages/ProductP';
import Logout from './pages/Logout';
import AddProduct from './pages/AddProducts';
import ViewGame from './pages/ViewGame';
import UpdateProduct from './pages/UpdateProduct';
import Cart from './pages/Cart';
import UserOrder from './pages/UserOrders';




import {Container} from 'react-bootstrap';

import './App.css'

export default function App() {

  let localCart = localStorage.getItem('cart');
  let cartData = [];
  try {

  const isValid = JSON.parse(localCart);

    if(isValid){

      cartData = isValid
    }
  }
  catch(e){

    console.log(e);
  }
  const [user,setUser] = useState({

    id: null,
    isAdmin: null,
    cart: cartData
  })

  function addToCart(product){

    // Check if the product is in the cart 
    // If its in the cart increase
    // Else Push into cart
    console.log("zzzzzz",product);

    const inCart = user.cart.find((p)=>{

      return product.id === p.id
    })
      if(inCart){

        const currentQuantity = inCart.quantity

        const otherCartItems = user.cart.filter((p)=> p.id !== product.id);

        setUser({

          ...user,
          cart: [...otherCartItems,{id: product.id, quantity: currentQuantity+1, price: product.price, name: product.name}]
        })
        
      } else {

        const otherCartItems = user.cart.filter((p)=> p.id !== product.id);
        setUser({

          ...user,
          cart: [...otherCartItems,{id: product.id, quantity: 1, price: product.price, name: product.name}]
        })
      }
  }

  function emptyCart(){

     setUser({

          ...user,
          cart: []
        })


  }

  useEffect(() => {

    localStorage.setItem('cart', JSON.stringify(user.cart))


  },[user])


  useEffect(() => {


    fetch('https://serene-woodland-75361.herokuapp.com/users/userDetails',{

      headers: {

        'Authorization' : `Bearer ${localStorage.getItem('token')}`
      }
    })

    .then(res => res.json())
    .then(data => {

      setUser({

        id: data._id,
        isAdmin: data.isAdmin,
        cart: [...user.cart]
      })
    })


  },[])

  const unsetUser = () => {

    localStorage.clear()

 
  }

  const getCart = () => {

    localStorage.getItem('cart');
  }

  return (

      <>
        <UserProvider value={{user,setUser,unsetUser,emptyCart,addToCart,getCart}}>
        <Router>
        <AppNavbar />
            <Container>
              <Switch>
                <Route exact path="/" component={Home}/>
                <Route exact path="/login" component={Login}/>
                <Route exact path="/register" component={Register}/>
                <Route exact path="/products" component={ProductP}/>
                <Route exact path="/products/:productId" component={ViewGame}/>
                <Route exact path="/updateProducts/:productId" component={UpdateProduct}/>
                <Route exact path="/logout" component={Logout}/>
                <Route exact path="/addProducts" component={AddProduct}/>
                <Route exact path="/cart" component={Cart}/>
                <Route exact path="/orders" component={UserOrder}/>
              </Switch>
            </Container>
        </Router>
        </UserProvider>

      </>

    )


}